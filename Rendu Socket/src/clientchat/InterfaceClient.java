/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientchat;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.DefaultCaret;

/**
 *
 * @author Corentin
 */
public class InterfaceClient extends javax.swing.JFrame {

    Socket SocketClient = null;
    ClientOut cOut;
    ClientIn cIn;
    boolean connecte;

    public InterfaceClient() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MessageTextField = new javax.swing.JTextField();
        BouttonEnvoyer = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        ChatArea = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        Menu = new javax.swing.JMenu();
        MenuItemConnexion = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ChatSocket GUI");

        MessageTextField.setText("Entrez votre message...");

        BouttonEnvoyer.setText("Envoyer");
        BouttonEnvoyer.setToolTipText("");
        BouttonEnvoyer.setEnabled(false);
        BouttonEnvoyer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BouttonEnvoyerActionPerformed(evt);
            }
        });

        jScrollPane2.setEnabled(false);

        ChatArea.setEditable(false);
        ChatArea.setColumns(20);
        ChatArea.setRows(5);
        jScrollPane2.setViewportView(ChatArea);

        Menu.setText("Fichier");

        MenuItemConnexion.setText("Connexion");
        MenuItemConnexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuItemConnexionActionPerformed(evt);
            }
        });
        Menu.add(MenuItemConnexion);

        jMenuBar1.add(Menu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane2)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(MessageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(BouttonEnvoyer, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(MessageTextField)
                                        .addComponent(BouttonEnvoyer))
                                .addContainerGap())
        );

        MessageTextField.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void MenuItemConnexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuItemConnexionActionPerformed
        InfosConnexion conForm = new InfosConnexion(this);
        conForm.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_MenuItemConnexionActionPerformed

    private void BouttonEnvoyerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BouttonEnvoyerActionPerformed
        if ("quit".equals(MessageTextField.getText())) {
            cOut.envoyer(MessageTextField.getText());
            MessageTextField.setText("");
            dispose();
        } else {
            cOut.envoyer(MessageTextField.getText());
            MessageTextField.setText("");
        }

    }//GEN-LAST:event_BouttonEnvoyerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfaceClient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfaceClient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfaceClient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfaceClient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                InterfaceClient c = new InterfaceClient();
                c.setLocationRelativeTo(null);
                c.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BouttonEnvoyer;
    private javax.swing.JTextArea ChatArea;
    private javax.swing.JMenu Menu;
    private javax.swing.JMenuItem MenuItemConnexion;
    private javax.swing.JTextField MessageTextField;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables

    /**
     * Méthode qui met à jour le chat avec les nouveaux messages
     *
     * @param readLine: chaine de caractères à afficher
     */
    public void afficher(String readLine) {
        ChatArea.setText(ChatArea.getText() + "\r\n" + readLine);
        DefaultCaret caret = (DefaultCaret) ChatArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    /**
     * Méthode permettant la connexion au serveur de chat. Initialise les objets
     * ClientIn et ClientOut
     *
     * @param pseudo: pseudo du client qui souhaite se connecter
     * @param host: adresse de l'hote
     * @param port: port de connexion
     * @throws IOException : peut déclencher une ereur d'entrée sortie à cause du socket
     */
    public void connexion(String pseudo, String host, int port) throws IOException {
        //Initialisation du socket
        SocketClient = new Socket(host, port);
        cOut = new ClientOut(SocketClient);
        cIn = new ClientIn(this, SocketClient);
        cIn.start();
        cOut.envoyer("join " + pseudo);
        BouttonEnvoyer.setEnabled(true);
        connecte = true;
    }

    /**
     * Permet de fermer correctement le socket lors de la fermeture.
     */
    @Override
    public void dispose() {
        if (connecte) {
            try {
                cIn.running = false;
                cOut.socOut.close();
                cIn.socIn.close();
                SocketClient.close();
            } catch (IOException ex) {
                Logger.getLogger(InterfaceClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        super.dispose();
    }
}
