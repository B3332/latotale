package serverchat;

import java.io.*;
import java.net.*;
import javax.swing.event.EventListenerList;

/**
 * @author vfalconieri Classe qui gère les inputs envoyées par le client,
 * jusqu'au serveur. Gère la récupération du pseudo, de la deconnexion du
 * serveur ...
 */
public class ThreadWaitClientInput extends Thread {

    private Socket clientSocket;
    private EntiteClient clientDeCeSocket;
    MainThread threadParent;

    /**
     * Constructeur qui récupère et stocke le Socket sur lequel il écoute/émet.
     *
     * @param s : le socket attribué au thread
     * @param threadParent : le thread qui a invoqué cet objet (thread lui
     * aussi)
     */
    ThreadWaitClientInput(Socket s, MainThread threadParent) {
        this.clientSocket = s;
        this.threadParent = threadParent;
    }

    /**
     * Thread principal de la classe, qui attend des messages entrants de la
     * part du client.
     *
     */
    public void run() {
        try {
            // On créé un flux d'entrée, qu'on lie avec le socket.
            BufferedReader socIn = null;
            socIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            // On créé un flux de sortie, qu'on lie avec le socket.
            PrintStream socOut = new PrintStream(clientSocket.getOutputStream());

            //On créé les variables nécessaire pour la récupération du pseudo
            String joinEtPseudo = socIn.readLine();
            String[] arguments = joinEtPseudo.split(" ");

            //On récupère le pseudo du client
            if (arguments.length == 2 && arguments[0].equals("join")) {

                // On créé le client
                clientDeCeSocket = new EntiteClient(arguments[1], socOut, socIn);

                // On ajoute l'objet ListeEntiteClient au serveur
                MainThread.listeEntiteClient.add(clientDeCeSocket);

                // On fait un affichage.
                socOut.println("Félicitation, vous vous êtes connecté.");

                // On lui fait récupérer l'historique des conversations
                MainThread.casterHistorique(clientDeCeSocket);

                //On lève un event dans le parent.
                //On averti le parent
                threadParent.notifierChatConnexionClient(clientDeCeSocket);

                //on notifie de la connexion du nouvel utilisateur
                MainThread.casterUnMessage("Serveur", arguments[1] + " s'est connecté");

            } else {
                socOut.println("Ciao.");
                // Renvoi une exception
                throw new Exception("Utilisateur ne veut pas faire 'join'.");
            }

            boolean estEnLigne = true;
            // On attend le besoin de l'émission.
            while (estEnLigne) {
                String line = socIn.readLine();

                switch (line) {
                    case "quit":
                        // On fait des affichages.
                        socOut.println("A bientôt! Vous vous êtes déconnecté.");
                        MainThread.casterUnMessage("Serveur", arguments[1] + " s'est déconnecté");

                        // On supprime l'user de la base.
                        MainThread.listeEntiteClient.remove(this.clientDeCeSocket);

                        //On averti le parent
                        threadParent.notifierChatDeconnexionClient(clientDeCeSocket);

                        //On fermes les flux puis le socket
                        this.clientDeCeSocket.fluxEntree.close();
                        this.clientDeCeSocket.fluxSortie.close();
                        clientSocket.close();

                        //On casse la boucle
                        estEnLigne = false;
                        break;
                    default:
                        MainThread.casterUnMessage(this.clientDeCeSocket.pseudo, line);
                        break;
                }
            }
        } catch (Exception e) {
            System.err.println("Error in ThreadWaitClientInput:" + e);
        }
    }
}
