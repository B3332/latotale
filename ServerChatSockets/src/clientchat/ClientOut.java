/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

/**
 *
 * @author Corentin
 */
public class ClientOut {

    PrintStream socOut = null;
    Socket SocketClient = null;

    /**
     * Constructeur de la classe ClientOut, pas forcement nécéssaire pour le
     * projet demandé mais utile pour de futures amélioration
     *
     * @param s : socket utilisé par le client
     * @throws IOException : peut déclencher une erreur d'entrée sortie à cause du socket
     */
    public ClientOut(Socket s) throws IOException {
        SocketClient = s;
        socOut = new PrintStream(SocketClient.getOutputStream());
    }

    /**
     * Méthode qui envoie des données sur le socket client.
     *
     * @param s: chaine de caractère à envoyer
     */
    public void envoyer(String s) {
        socOut.println(s);
    }
}
