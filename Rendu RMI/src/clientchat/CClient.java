package clientchat;

import java.rmi.RemoteException;

public class CClient implements CClientItf {
    ClientLauncher parent;

    /**
     *Constructeur de la classe CClient
     * @param parent: objet ayant appelé cette classe
     */
    public CClient(ClientLauncher parent) {
        this.parent = parent;
    }
    
    @Override
    public void recevoirMessage(String message) throws RemoteException {
        parent.afficher(message);
    }
}
