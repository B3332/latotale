package serverchat;

/**
 * Implémentation vide d'un listener de Chat, nécessaire pour la structure du programme. Evite qu'une classe implémentant l'interface ai besoin de définir TOUTES les méthodes.
 * Au final, retire une obligation.
 * @author Vincent FALCONIERI
 */
public abstract class E_ChatAdapter implements E_ChatListener {
	public void chatConnexionClient(E_ChatEvent e){}
	public void chatDeconnexionClient(E_ChatEvent e){}
	public void chatTexteServeur(E_ChatEvent e){}
}
