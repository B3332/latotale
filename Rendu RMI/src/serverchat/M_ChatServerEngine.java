package serverchat;

import clientchat.CClientItf;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.event.EventListenerList;

/**
 * Implémentation de l'interface envoyée au RMIregistry
 * Gère les clients qui se joignent, quittent, envoient des messages ...
 * @author Vincent FALCONIERI
 */
public class M_ChatServerEngine implements M_ChatServerEngineItf {

	/**
	 * Gestion de la liste des entités utilisateurs.
	 */
	static ArrayList<M_entiteClient> listeEntiteClient = new ArrayList<M_entiteClient>();

	/**
	 * Gestion des écrivains / lecteurs de logs
	 */
	static BufferedWriter logWriter = null;
	static BufferedReader logReader = null;

	// un seul objet pour tous les types d'écouteurs
	private final EventListenerList listeners = new EventListenerList();
        


	/**
	 * Constructeur du moteur du serveur de chat. Intialise les logs.
	 */
	public M_ChatServerEngine() {
		// On prépare le fichier de LOG
		try {
			logWriter = Files.newBufferedWriter(Paths.get("log.txt"), Charset.forName("UTF-8"));
			logReader = Files.newBufferedReader(Paths.get("log.txt"), Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.err.println("Error in ServeurChat Constructeur :" + e);
		}
	}

        @Override
	public boolean joinChat(CClientItf clientItf, String pseudo) throws RemoteException  {
		boolean answer = false;

		try {
			// On créé le client
			M_entiteClient entiteTMP = new M_entiteClient(pseudo, clientItf);

			// On lui envoit l'historique
			casterHistorique(clientItf);

			// On lui donne la liste des connecté
			if (listeEntiteClient.size() != 0) {
				clientItf.recevoirMessage("SERVEUR > La liste des connecté est : " + getListeConnecte());
			} else {
				clientItf.recevoirMessage("SERVEUR > Il n'y a aucun connecté");
			}

			// On l'ajoute à la liste des clients et on donne le nombre de
			// client.
			listeEntiteClient.add(entiteTMP);

			// Un nouveau est là
			System.out.println("Nouveau Client " + pseudo);
			
			//On notifie la fenêtre parente
			firechatConnexionClient("Nouveau Client " + pseudo);

			answer = true;
		} catch (Exception e) {
			System.err.println("Error in ServeurChat Join :" + e);
		}

		return answer;
	}

        @Override
	public boolean sendMessage(CClientItf clientItf, String message)  throws RemoteException {
		boolean answer = true;

		// On récupère l'entité client correspondante
		M_entiteClient entiteTMP = getClient(clientItf);
		String pseudo = "inconnu";

		// Si il est bien dans la liste, en l'enlève.
		if (entiteTMP != null) {
			pseudo = entiteTMP.pseudo;
		}

		// On envoit le message à tout le monde.
		answer = sendMessage(pseudo, message);

		return answer;
	}

        @Override
	public boolean quitChat(CClientItf clientItf) throws RemoteException  {

		// On récupère l'entité client correspondante
		M_entiteClient entiteTMP = getClient(clientItf);

		// Si il est bien dans la liste, en l'enlève.
		if (entiteTMP != null) {
			// On récupère son pseudo.
			String pseudo = entiteTMP.pseudo;

			// On broadcast un message de déconnexion.
			sendMessage("SERVEUR", pseudo + " s'est déconnecté.");

			// On supprime l'user de la base et on met à jour le nombre d'user
			listeEntiteClient.remove(entiteTMP);
			
			//On averti les parents
			firechatDeconnexionClient(pseudo + " s'est déconnecté.");

			return true;
		}

		return false;
	}

	/** Permet de récupérer l'identité d'un client depuis sa signature d'interface
	 * @param clientItf : son interface
	 * @return : l'identité du client correspondant
	 */
	public M_entiteClient getClient(CClientItf clientItf) {
		// On parcours la liste
		for (int i = 0; i < listeEntiteClient.size(); i++) {
			// Si l'un de ses attributs correspondent
			if (listeEntiteClient.get(i).skeletonClient.equals(clientItf)) {
				// On le retourne
				return listeEntiteClient.get(i);
			}
		}
		return null;
	}

	/**
	 * Permet de récupérer l'historique des conversations
         * @param clientItf : Le client à qui on désire caster l'histoirque. Doit avoir rejoint la session précédemment ! 
	 */
	public static void casterHistorique(CClientItf clientItf) {
		// NOTE : on pourrait limiter le nombre de lignes renvoyées .. mais ce
		// n'est pas l'objet du TP. (Vision grande échelle)

		try {

			// Some greetings
			clientItf.recevoirMessage("== Récupération de l'historique de conversation ==");

			// On récupère tout le contenu
			String line = "";
			while ((line = logReader.readLine()) != null) {
				clientItf.recevoirMessage(line);
			}

			// On salut
			clientItf.recevoirMessage("== Fin de la récupération de l'historique de conversation ==");
		} catch (Exception e) {
			System.err.println("Error in ServeurChat cast Historique :" + e);
		}
	}

	/** Permet d'envoyer un message à l'ensemble des clients, avec le pseudo spécifié.
	 * @param pseudo : Le pseudo de celui qui envoit le message
	 * @param message : le message en question qui sera broadcasté
	 * @return : true si l'envoi s'est bien déroulé, false si il y a eu une erreur
	 */
	private boolean sendMessage(String pseudo, String message) {
		boolean answer = true;

		// On constitue la chaîne à Broadcast/mettre en historique/etc.
		String txtDate = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE).format(new Date());
		String tmpToBroadcast = txtDate + "| " + pseudo + "> " + message;

		// On écrit dans les logs
		try {
			logWriter.append(tmpToBroadcast); // On ajoute la ligne aux logs
			logWriter.newLine(); // Aller à la ligne suivante
			logWriter.flush(); // libère le buffer
		} catch (Exception e) {
			System.err.println("Error in ServeurChat :" + e);
		}

		// On broadcast le message à tout le monde en spécifiant le pseudo
		for (int i = 0; i < listeEntiteClient.size(); i++) {
			try {
				CClientItf htmp = (CClientItf) listeEntiteClient.get(i).skeletonClient;
				htmp.recevoirMessage(tmpToBroadcast);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				System.err.println("Error in ServeurChat - sendMessage Join N° " + i + " :" + e);
				answer = false;
			}
		}
		return answer;
	}

	/** Permet de récupérer la liste des connectés au serveur actuellement
	 * @return : une String, avec la liste des connectés séparés par une virgule.
	 */
	private String getListeConnecte() {
		String liste = "";

		for (int i = 0; i < listeEntiteClient.size(); i++) {
			liste += listeEntiteClient.get(i).pseudo;
			if (i != listeEntiteClient.size() - 1) {
				liste += ", ";
			}
		}

		return liste;
	}

	public void addChatListener(E_ChatListener listener) {
		listeners.add(E_ChatListener.class, listener);
	}

	public void removeChatListener(E_ChatListener listener) {
		listeners.remove(E_ChatListener.class, listener);
	}

	/**
	 * @return : la liste des listeners de cette classe
	 */
	public E_ChatListener[] getChatListener() {
		return listeners.getListeners(E_ChatListener.class);
	}

	/** Permet de notifier les écouteurs de la connexion d'un client
	 * @param message : Le message à transférer aux écouteurs
	 */
	protected void firechatConnexionClient(String message) {
		E_ChatEvent e = new E_ChatEvent(message);
                
		for (E_ChatListener listener : getChatListener()) {
			listener.chatConnexionClient(e);
		}
	}
	
	/** Permet de notifier les écouteurs de la déconnexion d'un client
	 * @param message : Le message à transférer aux écouteurs
	 */
	protected void firechatDeconnexionClient(String message) {
		E_ChatEvent e = new E_ChatEvent(message);

		for (E_ChatListener listener : getChatListener()) {
			listener.chatDeconnexionClient(e);
		}

	}

	/** Permet de notifier les écouteurs d'un texte que le serveur veut délivrer
	 * @param message : Le message à transférer aux écouteurs
	 */
	protected void firechatTexteServeur(String message) {
		E_ChatEvent e = new E_ChatEvent(message);

		for (E_ChatListener listener : getChatListener()) {
			listener.chatTexteServeur(e);
		}
	}

}
