package serverchat;

import clientchat.CClientItf;
import java.io.BufferedReader;
import java.io.PrintStream;

/** 
 * Gère un client. On retient son pseudo ainsi que son skeleton, associé.
 * Permet des appels de méthodes remote à ce client, en particulier.
 * @author Vincent FALCONIERI
 */
public class M_entiteClient {
	// Objets directement construits
	public String pseudo = "";
	public CClientItf skeletonClient = null;

	// Constructeur
	M_entiteClient(String pseudo, CClientItf interfaceClient) {
		this.pseudo = pseudo;
		this.skeletonClient = interfaceClient;
	}
}