package clientchat;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CClientItf extends Remote {

    /**
     * Méthode mettant à jour l'interface client pour la reception d'une infos
     * depuis le serveur
     *
     * @param message: chaine de caractère à afficher
     * @throws RemoteException : Erreur pouvant être envoyée à l'envoi d'un message
     */
    public void recevoirMessage(String message) throws RemoteException;
}
