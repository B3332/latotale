package serverchat;

import java.util.EventListener;

/**
 * Les listeners qui doivent être implémentés si on veut écouter un "Serveur de
 * chat"
 *
 * @author Vincent FALCONIERI
 */
public interface E_ChatListener extends EventListener {

    /**
     * Methode pour notifier d'une connexion client, comme évènement.
     *
     * @param e : l'évènement
     */
    void chatConnexionClient(E_ChatEvent e);

    /**
     * Methode pour notifier d'une déconnexion client, comme évènement.
     *
     * @param e : l'évènement
     */
    void chatDeconnexionClient(E_ChatEvent e);

    /**
     * Methode pour notifier d'un texte voulant être affiché sur la console,
     * comme évènement.
     *
     * @param e : l'évènement
     */
    void chatTexteServeur(E_ChatEvent e);
}
