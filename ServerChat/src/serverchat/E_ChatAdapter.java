package serverchat;

/**
 * Implémentation vide d'un listener de Chat, nécessaire pour la structure du programme. Evite qu'une classe implémentant l'interface ai besoin de définir TOUTES les méthodes.
 * Au final, retire une obligation.
 * @author Vincent FALCONIERI
 */
public abstract class E_ChatAdapter implements E_ChatListener {

    /**
     * Methode pour notifier d'une connexion client, comme évènement.
     * @param e : l'évènement
     */
    public void chatConnexionClient(E_ChatEvent e){}
  
    /**
     * Methode pour notifier d'une déconnexion client, comme évènement.
     * @param e : l'évènement
     */
    public void chatDeconnexionClient(E_ChatEvent e){}

    /**
     * Methode pour notifier d'un texte voulant être affiché sur la console, comme évènement.
     * @param e : l'évènement
     */
    public void chatTexteServeur(E_ChatEvent e){}
}
