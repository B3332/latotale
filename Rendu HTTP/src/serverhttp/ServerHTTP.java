///A Simple Web Server (WebServer.java)
package serverhttp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe permettant de gérer le serveur HTML
 *
 * @author Vincent FALCONIERI et Corentin GIRAUD
 */
public class ServerHTTP {

    static ArrayList<File> listeFichiersServeur = new ArrayList<File>();

    /**
     * Gestion des écrivains / lecteurs de logs
     */
    static BufferedReader pageReader = null;

    // Valeurs utiles :
    static int PORT = 3000;
    static String versionHTTP = "HTTP/1.0";
    static String versionServer = "Hapash/0.0.1";
    static String repertoireServer = "ResTest/";
    //static File repertoireServeur = new File("Ressources/");
    static File repertoireServeur = new File(repertoireServer);
    static File fichier404 = new File(repertoireServer + "404.html");
    static File fichier403 = new File(repertoireServer + "403.html");
    static File ressourceParDefaut = new File(repertoireServer + "index.html"); //Page par défaut
    static String stringVide = "";
    static PrintWriter out;
    static OutputStream outRaw;

    private static String cheminToWindows(String chemin) {
        //A activer si on compile pour windows.
//        chemin = chemin.replace("/", "\\");
        return chemin;
    }

    /**
     * * WebServer constructor.
     */
    protected void start() {
        //On ouvre le socket
        ServerSocket s;

        //On liste le contenu du repértoire du serveur
        crawlRepertoire(repertoireServeur);

        //On ouvre le lecteur de page web
        try {
            //pageReader = Files.newBufferedReader(Paths.get("index.html"), Charset.forName("UTF-8"));
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return;
        }

        // create the main server socket
        System.out.println("Webserver starting up on port : " + PORT);
        System.out.println("(press Ctrl-C to exit)");
        try {
            // create the main server socket
            s = new ServerSocket(PORT);
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return;
        }
        Socket remote = null;
        BufferedReader in = null;
        System.out.println("Waiting for connection");
        for (;;) {
            try {
                // wait for a connection
                remote = s.accept();
                // remote is now the connected socket
//                System.out.println("Connection, sending data.");
                in = new BufferedReader(new InputStreamReader(
                        remote.getInputStream()));
                out = new PrintWriter(remote.getOutputStream());
                outRaw = remote.getOutputStream();

                decode(in);
                out.flush();

                remote.close();
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }
        }
    }

    /**
     * Start the application.
     *
     * @param args Command line parameters are not used.
     */
    public static void main(String args[]) {
        ServerHTTP ws = new ServerHTTP();
        ws.start();
    }

    /**
     * Methode gérant l'envoi des données. Permet de facilement switcher d'un
     * mode "texte" à un mode "raw"
     *
     * @param chaine : la chaine de caractères à envoyer.
     */
    public static void appendOut(String chaine) {
        //DEBUG // System.out.println(chaine);

        //Ecrie en texte : 
        //out.println(chaine);
        try {
            //Ecrire en byte directement : 
            outRaw.write(chaine.getBytes());
            outRaw.write("\n".getBytes());
        } catch (IOException ex) {
            Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Methode permettant de récupérer tout le contenu d'une repertoire et le
     * parser dans un array
     *
     * @param repertoire : le répertoire à parser.
     */
    public static void crawlRepertoire(File repertoire) {

        if (repertoire.isDirectory()) {
            File[] list = repertoire.listFiles();
            if (list != null) {
                for (int i = 0; i < list.length; i++) {
                    // Appel récursif sur les sous-répertoires
                    crawlRepertoire(list[i]);
                }
            } else {
                System.err.println(repertoire + " : Erreur de lecture.");
            }
        } else {
            String cheminFichierActuel = repertoire.getAbsolutePath();
            cheminFichierActuel = cheminToWindows(cheminFichierActuel); // Si le serveur est éxécuter sur une machine windows
            listeFichiersServeur.add(new File(cheminFichierActuel));
            System.out.println(" On tente d'ajouter à la liste des fichiers: " + cheminFichierActuel);
        }
    }

    public static void decode(BufferedReader in) {

        try {

            String reqHeader = stringVide;
            String reqBody = stringVide;

            String tmp = stringVide;

            //On parse l'entrée
            while (tmp != null) {
                tmp = in.readLine();
                if (tmp.isEmpty()) {
                    break;
                }
                reqHeader = reqHeader + tmp;
            }

            //On voit qu'on peut savori si il y adu contenu.
            if (reqHeader.indexOf("application/x-www-form-urlencoded") != -1) {
                System.out.println("Il y a du contenu ! ");
            }

            //NOTE : on pourrait lire caractère par caractère ?
            //On l'affiche dans la conseol pour débugging
            System.out.println(reqHeader);
            System.out.println(reqBody);

            String[] arg = reqHeader.split(" ");

            switch (arg[0]) {
                case "GET":
                    reponseGet(arg);
                    break;
                case "POST":
                    StringBuilder payload = new StringBuilder();
                    while (in.ready()) {
                        payload.append((char) in.read());
                    }
                    System.out.println(payload.toString());
                    reponsePost(payload.toString());
                    break;
                case "HEAD":
                    reponseHead(arg);
                    break;
                default:
                    reponseErreur();
            }

        } catch (IOException ex) {
            Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Gère une réponse à une requête mal formée (Erreur 403)
     */
    public static void reponseErreur() {
        String rep;

        //Version HTTP
        rep = "HTTP/1.0 403 BAD REQUEST";
        appendOut(rep);

        //Date
        rep = "Date: " + getServerTime();
        appendOut(rep);

        //Server + version
        rep = "Server: Hapash/0.0.1";
        appendOut(rep);
        sendFileInformation(fichier403);

        //Ligne vide
        appendOut(stringVide);

        sendFileContent(fichier403);

        System.out.println("Page d'erreur envoyée");
    }

    /**
     * Permet de récupérer la référence sur le fichier passé en paramètre, si il
     * est dans le répertoire
     *
     * @param nomFichier : Nom du fichier voulant être trouvé
     * @return : null si on ne le trouve pas, le fichier en question si on l'a
     * trouvé
     */
    public static File getFichier(String nomFichier) {
        //Note, normalement le nom du fichier comporte déjà le "/"
        String aRechercher = repertoireServeur.getAbsolutePath() + nomFichier;
        //Affichage de DEBUG
        aRechercher = cheminToWindows(aRechercher); // Si le serveur est éxécuter sur une machine windows
//        System.out.println(" On tente de trouver le fichier: " + aRechercher);

        File answer = null;

        for (int i = 0; i < listeFichiersServeur.size(); i++) {
            if (listeFichiersServeur.get(i).getAbsolutePath().equals(aRechercher)) {
//                System.out.println(" On a trouvé le fichier: " + listeFichiersServeur.get(i).getAbsolutePath());
                answer = listeFichiersServeur.get(i);
                break;
            }
        }

        return answer;
    }

    /**
     * Méthode permettant de répondre à une requête HTTP GET
     *
     * @param args : les différents paramètres d'une requête HTTP parsés.
     */
    public static void reponseGet(String args[]) {
        String code, message;
        String answerCourante = "";
        File requeteFichier;

        //On créé le Header de la réponse
        reponseHead(args);

        //On gère le cas où on ne spécifie pas de fichier particulier
        if (args[1].equals("/")) {
            requeteFichier = ressourceParDefaut;
        } else {
            requeteFichier = getFichier(args[1]);
        }
        //On gère le cas où la page demandé n'existe pas.
        if (requeteFichier == null) {
            // On dit que le fichier courant est la page 404
            requeteFichier = fichier404;
        }

        //On envoit le contenu du fichier
        sendFileContent(requeteFichier);
    }

    /**
     * Méthode n'envoyant que le headeret non le contenu, losrs d'une requête
     *
     * @param args : les paramètres de la requête HTTP entrante.
     */
    public static void reponseHead(String args[]) {
        String code, message;
        String answerCourante = stringVide;
        File requeteFichier;

        if (args[1].equals("/")) {
            requeteFichier = ressourceParDefaut;
        } else {
            requeteFichier = getFichier(args[1]);
        }

        if (requeteFichier != null) {
            //On génère le header
            code = " 200";
            message = " OK";
            answerCourante = versionHTTP + code + message;
        } else {
            //On génère le header
            code = " 404";
            message = " NOT FOUND";
            answerCourante = versionHTTP + code + message;

            //On envoit les informations du fichier
            requeteFichier = fichier404;
        }
        appendOut(answerCourante);

        //On envoit le moment de la réponse
        appendOut(getServerTime());
        //On envoit les informations du serveur
        appendOut(getServerVersion());
        //On envoit les informations du fichier
        sendFileInformation(requeteFichier);

    }

    /**
     *
     * Méthode pour traiter les requêtes utilisant la méthode POST, génère un
     * page html.
     *
     * @param chaineBrute : les paramètres de la requête HTTP entrante.
     *
     */
    public static void reponsePost(String chaineBrute) {
        
        //DEBUG // 
        System.out.println("Voici la chaine brut entrante dans POST : " + chaineBrute);
        //On créé un fileoutpstream pour écrire dans un fichier.
        FileOutputStream fos = null;
        try {
            //On parse l'entrée
            String[] v = chaineBrute.split("&");
            String[][] variables = new String[v.length][2];

            //On stocke les arguments
            for (int i = 0; i < v.length ; i++) {
                String[] v_tmp = v[i].split("=");
                variables[i][0] = v_tmp[0];
                variables[i][1] = v_tmp[1];
            }
            File Fichier = new File(repertoireServer + "reponseFormulaire.html");

            //On créé le fichier
            fos = new FileOutputStream(Fichier);
            //On créé la page
            String page = "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "  <head>\n"
                    + "    <title>Résultats Formulaire</title>\n"
                    + "  </head>\n"
                    + "  <body>\n"
                    + "<table>\n"
                    + "   <tr>\n"
                    + "       <th>Variables</th>\n"
                    + "       <th>Valeur(s)</th>\n"
                    + "   </tr>";
            //On créé le tableau
            for (int i = 0; i < v.length; i++) {
                page = page + "<tr>\n"
                        + "<td>\n"
                        + variables[i][0]
                        + "\n</td>\n"
                        + "<td>\n"
                        + variables[i][1]
                        + "\n</td>\n"
                        + "</tr>\n";
            }
            //On termine la page
            page = page + "</table>\n"
                    + "  </body>\n"
                    + "</html>";
            //On écrit dans le fichier
            byte[] b = page.getBytes();
            fos.write(b);
            fos.close();

            //On génère la réponse HTTP
            //On génère le header
            appendOut(versionHTTP + " 200" + " OK");
            //On envoit le moment de la réponse
            appendOut(getServerTime());
            //On envoit les informations du serveur
            appendOut(getServerVersion());
            //Location
            appendOut("Location: formulaire.html");
            appendOut(stringVide);

            sendFileContent(Fichier);
        } catch (IOException ex) {
            Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Méthode permettant d'obtenir la version du serveur
     *
     * @return : une String contenu la version du serveur, formatée
     */
    public static String getServerVersion() {
        return "Server:" + versionServer;
    }

    /**
     * Methode permettant d'envoyer sur le PrinWriter donné en paramètre,
     * l'entête correspondant à la lecture d'un fichier spécifique.
     *
     * @param requeteFichier : le fichier sur lequel on récupère les
     * informations
     */
    public static void sendFileInformation(File requeteFichier) {
        String answerTMP;
        String nomFichier = requeteFichier.getName();

        // On gère le content type
        answerTMP = "Content-Type: ";
        if (nomFichier.endsWith(".html") || nomFichier.endsWith(".txt")) {
            answerTMP += "text/html";
        } else if (nomFichier.endsWith(".jpg") || nomFichier.endsWith(".png") || nomFichier.endsWith(".gif") || nomFichier.endsWith(".bmp")) {
            answerTMP += "image/gif";
        } else if (nomFichier.endsWith(".css")) {
            answerTMP += "text/css";
        } else {
            answerTMP += "unknown";
        }
        //On envoit
        appendOut(answerTMP);

        // On gère le Content Length
        long length = requeteFichier.length();
        answerTMP = "Content-Length: " + Long.toString(length);
        appendOut(answerTMP);

        // On gère le expires
        answerTMP = "Expires: " + getExpiresTime(15); //A MODIFIER
        appendOut(answerTMP);

        // On gère le last Modified
        answerTMP = "Last-modified: " + new Date(requeteFichier.lastModified());
        appendOut(answerTMP);

    }

    /**
     * Permet d'envoyer le contenu d'un fichier : ligne par ligne si c'est du
     * texte, en byte sinon.
     *
     * @param requeteFichier : le fichier à envoyer
     */
    public static void sendFileContent(File requeteFichier) {
        String nomFichier = requeteFichier.getName();

        if (nomFichier.endsWith(".html") || nomFichier.endsWith(".txt")) {
            //C'est un fichier texte
            try {
                appendOut(stringVide);
                BufferedReader in = new BufferedReader(new FileReader(requeteFichier));
                String line;
                while ((line = in.readLine()) != null) {
                    // Afficher le contenu du fichier
                    appendOut(line);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (nomFichier.endsWith(".jpg") || nomFichier.endsWith(".png") || nomFichier.endsWith(".gif") || nomFichier.endsWith(".bmp")) {
            appendOut(stringVide);

            try {
                //C'est une image
                outRaw.write(Files.readAllBytes(requeteFichier.toPath()));
                System.out.println("On envoit une image depuis : " + requeteFichier.toPath());
            } catch (IOException ex) {
                Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            appendOut(stringVide);

            //C'est n'importe quoi d'autres // Faute de mieux, on fait comme les images
            try {
                //C'est une image
                outRaw.write(Files.readAllBytes(requeteFichier.toPath()));
            } catch (IOException ex) {
                Logger.getLogger(ServerHTTP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Récupérer le temps local du serveur
     *
     * @return : Une String correspondant eu temps local.
     */
    public static String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GTM"));
        return dateFormat.format(calendar.getTime());
    }

    /**
     * Permet de récupérer une date d'expiration, en fonction du temps local du
     * serveur
     *
     * @param nbMinute : le nombre de minute avant l'expriation, sera ajouté au
     * temps local
     * @return : le temps correspondant à l'expiration
     */
    public static String getExpiresTime(int nbMinute) {
        Date expdate = new Date();
        expdate.setTime(expdate.getTime() + (3600 * 60 * nbMinute));
        DateFormat df = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss zzz");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String cookieExpire = df.format(expdate);
        return cookieExpire;
    }
}
